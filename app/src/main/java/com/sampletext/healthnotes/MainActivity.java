package com.sampletext.healthnotes;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    Button m_btnToOverview, m_btnToAuth;

    View.OnClickListener toOverviewClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(getApplicationContext(), SelectDataTypeActivity.class);
            startActivity(intent);
        }
    };
    View.OnClickListener toAuthClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(getApplicationContext(), AuthActivity.class);
            startActivity(intent);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Для тестов базу можно полностью очищать при старте приложения
        //this.deleteDatabase(DbManager.DATABASE_NAME);

        //Если база пустая - создать в ней стандартного пользователя и тестовые записи
        if(!DbManager.getInstance(this).findUser("admin", "admin")) {
            DbManager.getInstance(this).insertUser("admin", "admin");
            DbManager.getInstance(this).insertDisease("Болезнь", "Это нарушение нормальной жизнедеятельности организма, обусловленное функциональными или (и) морфологическими изменениями, возникающие вследствие воздействия на организм вредных факторов внешней среды (физических, химических, биологических, социальных).");
            DbManager.getInstance(this).insertMedicine("Лекарство", "Вещество или смесь веществ синтетического или природного происхождения в виде лекарственной формы (таблетки, капсулы, растворы, мази и т. п.), применяемые для профилактики, диагностики и лечения заболеваний.");

        }

        m_btnToOverview = findViewById(R.id.buttonToOverview);
        m_btnToAuth = findViewById(R.id.buttonToAuth);

        m_btnToOverview.setOnClickListener(toOverviewClickListener);
        m_btnToAuth.setOnClickListener(toAuthClickListener);
    }
}
