package com.sampletext.healthnotes;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class SelectDataTypeActivity extends AppCompatActivity {

    Button m_btnToDiseases, m_btnToMedicine;

    //Кнопка болезней
    View.OnClickListener toDiseasesClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(getApplicationContext(), DisplayDataActivity.class);
            intent.putExtra("type", "diseases");
            startActivity(intent);
        }
    };

    //Кнопка лекарств
    View.OnClickListener toMedicineClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(getApplicationContext(), DisplayDataActivity.class);
            intent.putExtra("type", "medicines");
            startActivity(intent);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_data_type);
        m_btnToDiseases = findViewById(R.id.buttonToDiseases);
        m_btnToMedicine = findViewById(R.id.buttonToMedicine);

        m_btnToDiseases.setOnClickListener(toDiseasesClickListener);
        m_btnToMedicine.setOnClickListener(toMedicineClickListener);
    }
}
