package com.sampletext.healthnotes;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;

public class DataItemAdapter extends BaseExpandableListAdapter {
    private ArrayList<DataItem> mItems;//отображаемые элементы
    private Context mContext;//создающий контекст
    private String mContentType;//тип контента

    //обработчик кнопки изменения
    private View.OnClickListener buttonEditOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int position = (int) v.getTag();
            DataItem dataItem = mItems.get(position);
            Intent intent = new Intent(mContext, DataActivity.class);
            intent.putExtra("type", mContentType);
            intent.putExtra("action", "edit");
            intent.putExtra("id", dataItem.getId());
            mContext.startActivity(intent);
        }
    };

    public DataItemAdapter(Context context, ArrayList<DataItem> items) {
        mContext = context;
        mItems = items;
    }

    @Override
    public int getGroupCount() {
        return mItems.size();
    }

    //все элементы имеют по 1 подэлементу
    @Override
    public int getChildrenCount(int groupPosition) {
        return 1;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return mItems.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return mItems.get(groupPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return 1;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    //обработчик создания заголовка
    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.list_row_element_layout, null);
        }
        TextView textGroup = convertView.findViewById(R.id.textViewHeader);
        textGroup.setText(mItems.get(groupPosition).getName());

        Button buttonEditItem = convertView.findViewById(R.id.buttonEditItem);
        buttonEditItem.setTag(groupPosition);//устанавливаем кнопке тег - номер элемента в списке
        buttonEditItem.setOnClickListener(buttonEditOnClickListener);

        //если Админ - включаем кнпоку изменения
        if (Global.isAdmin) {
            buttonEditItem.setVisibility(View.VISIBLE);
        }

        return convertView;
    }

    //обработчик создания подэлемента
    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.list_row_content_layout, null);
        }

        TextView textChild = convertView.findViewById(R.id.textViewContent);
        textChild.setText(mItems.get(groupPosition).getContent());

        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }

    public String getContentType() {
        return mContentType;
    }

    public void setContentType(String contentType) {
        this.mContentType = contentType;
    }
}
