package com.sampletext.healthnotes;

public class DataItem {
    private int mId;
    private String mName;
    private String mContent;

    public DataItem(int id, String name, String content) {
        mId = id;
        mName = name;
        mContent = content;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        this.mName = name;
    }

    public String getContent() {
        return mContent;
    }

    public void setContent(String content) {
        this.mContent = content;
    }

    public int getId() {
        return mId;
    }

    public void setId(int mId) {
        this.mId = mId;
    }
}