package com.sampletext.healthnotes;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class DataActivity extends AppCompatActivity {
    TextView textViewHeader;//заголовк
    EditText editTextName, editTextContent;//поля ввода
    Button buttonDelete, buttonSave;//кнопки

    String mContentType;//текущий тип контента
    String mAction;//текущий вид действия
    int mId;//текущий ID записи

    DataItem displayingItem;//текущая запись

    //обработчик удаления
    View.OnClickListener buttonDeleteOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            AlertDialog.Builder builder = new AlertDialog.Builder(DataActivity.this);
            if (mContentType.equals("diseases")) {
                DbManager.getInstance(getApplicationContext()).deleteDisease(mId);
                builder.setTitle("Болезнь была удалена");
            } else if (mContentType.equals("medicines")) {
                DbManager.getInstance(getApplicationContext()).deleteMedicine(mId);
                builder.setTitle("Лекарство было удалено");
            }
            builder.setPositiveButton("Возврат", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    finish();
                }

            });
            builder.create().show();
        }
    };

    //обработчик сохранения
    View.OnClickListener buttonSaveOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            AlertDialog.Builder builder = new AlertDialog.Builder(DataActivity.this);

            String name = editTextName.getText().toString();
            String content = editTextContent.getText().toString();
            //проверяем пустоту
            if (name.length() == 0) {
                Toast.makeText(getApplicationContext(), "Не заполнено название", Toast.LENGTH_SHORT).show();
            }
            //если добавляем
            if (mAction.equals("add")) {
                if (mContentType.equals("diseases")) {
                    DbManager.getInstance(getApplicationContext()).insertDisease(name, content);
                    builder.setTitle("Болезнь была сохранена");
                } else if (mContentType.equals("medicines")) {
                    DbManager.getInstance(getApplicationContext()).insertMedicine(name, content);
                    builder.setTitle("Лекарство было сохранено");
                }
            }//если изменяем
            else if (mAction.equals("edit")) {
                //если поля не изменились, то нет смысла их обновлять
                if (displayingItem.getName().equals(name) && displayingItem.getContent().equals(content)) {
                    Toast.makeText(getApplicationContext(), "Поля не были изменены", Toast.LENGTH_SHORT).show();
                } else {
                    //меняем данные
                    displayingItem.setName(name);
                    displayingItem.setContent(content);
                    //обновляем
                    if (mContentType.equals("diseases")) {
                        DbManager.getInstance(getApplicationContext()).updateDisease(displayingItem);
                        builder.setTitle("Болезнь была изменена");
                    } else if (mContentType.equals("medicines")) {
                        DbManager.getInstance(getApplicationContext()).updateMedicine(displayingItem);
                        builder.setTitle("Лекарство было изменено");
                    }
                }
            }
            builder.setPositiveButton("Возврат", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    finish();
                }

            });
            builder.create().show();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data);

        mContentType = getIntent().getStringExtra("type");
        mAction = getIntent().getStringExtra("action");
        mId = getIntent().getIntExtra("id", -1);

        textViewHeader = findViewById(R.id.textViewHeader);

        editTextName = findViewById(R.id.editTextName);
        editTextContent = findViewById(R.id.editTextContent);

        buttonDelete = findViewById(R.id.buttonDelete);
        buttonSave = findViewById(R.id.buttonSave);

        buttonDelete.setOnClickListener(buttonDeleteOnClickListener);
        buttonSave.setOnClickListener(buttonSaveOnClickListener);

        //заполнение полей при запуске
        if (mAction.equals("add")) {
            if (mContentType.equals("diseases")) {
                textViewHeader.setText("Добавление болезни");
            } else if (mContentType.equals("medicines")) {
                textViewHeader.setText("Добавление лекарства");
            }
            buttonDelete.setVisibility(View.INVISIBLE);//при добавлении мы не можем удалить запись
        } else if (mAction.equals("edit")) {
            if (mContentType.equals("diseases")) {
                textViewHeader.setText("Изменение болезни");
                DataItem disease = DbManager.getInstance(this).getDiseaseById(mId);
                if (disease != null) {
                    displayingItem = disease;
                    editTextName.setText(disease.getName());
                    editTextContent.setText(disease.getContent());
                }
            } else if (mContentType.equals("medicines")) {
                textViewHeader.setText("Изменение лекарства");
                DataItem medicine = DbManager.getInstance(this).getMedicineById(mId);
                if (medicine != null) {
                    displayingItem = medicine;
                    editTextName.setText(medicine.getName());
                    editTextContent.setText(medicine.getContent());
                }
            }
        }


    }
}
