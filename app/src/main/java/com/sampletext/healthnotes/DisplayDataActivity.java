package com.sampletext.healthnotes;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ExpandableListView;

import java.util.ArrayList;

public class DisplayDataActivity extends AppCompatActivity {
    ExpandableListView mListView;//список
    EditText editTextSearch;//строка поиска
    FloatingActionButton floatingActionButton;//кнопка добавления
    String mContentType;//тип отображаемого контента (болезни или лекарства)

    String currentSearchText = "";//текущая строка поиска

    //обработчик кнопки добавления
    View.OnClickListener fabOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(getApplicationContext(), DataActivity.class);
            intent.putExtra("type", mContentType);
            intent.putExtra("action", "add");
            intent.putExtra("id", 0);
            startActivity(intent);
        }
    };

    //обработчик поиска
    TextWatcher searchOnTextChangedListener = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            currentSearchText = s.toString();
            fillList();
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    //этот метод вызывается, когда происходит возврат в Activity из дочерней
    @Override
    protected void onResume() {
        super.onResume();
        fillList();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_data);

        mContentType = getIntent().getStringExtra("type");//получаем тип

        mListView = findViewById(R.id.expandableListView);
        editTextSearch = findViewById(R.id.editTextSearch);
        editTextSearch.addTextChangedListener(searchOnTextChangedListener);

        fillList();//заполняем список

        floatingActionButton = findViewById(R.id.fab);

        floatingActionButton.setOnClickListener(fabOnClickListener);

        //если админ - показываем кнопку добавления
        if (Global.isAdmin) {
            floatingActionButton.show();
        } else {
            floatingActionButton.hide();
        }
    }

    private void fillList() {
        ArrayList<DataItem> items;
        //если поиск пустой, заполняем всё, иначе заполняем по поиску
        switch (mContentType) {
            case "diseases":
                if (currentSearchText.length() == 0) {
                    items = DbManager.getInstance(this).diseasesSelectAll();
                } else {
                    items = DbManager.getInstance(this).diseasesSelectWhereName(currentSearchText);
                }
                break;
            case "medicines":
                if (currentSearchText.length() == 0) {
                    items = DbManager.getInstance(this).medicinesSelectAll();
                } else {
                    items = DbManager.getInstance(this).medicinesSelectWhereName(currentSearchText);
                }
                break;
            default:
                items = new ArrayList<>();
                break;
        }
        DataItemAdapter itemAdapter = new DataItemAdapter(this, items);//создаём адаптер (важно! передавать контекст this иначе не будут работать кнопки внутри)
        itemAdapter.setContentType(mContentType);//устанавливаем тип контента (необходимо для кнопок внутри)
        mListView.setAdapter(itemAdapter);
    }
}
