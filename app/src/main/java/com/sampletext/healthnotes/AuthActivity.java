package com.sampletext.healthnotes;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class AuthActivity extends AppCompatActivity {
    EditText editTextViewLogin, editTextViewPassword;
    Button buttonAuth;

    //обработчик нажатия на вход
    View.OnClickListener buttonAuthOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            String login = editTextViewLogin.getText().toString();
            String password = editTextViewPassword.getText().toString();
            //если нашли пользователя
            if (DbManager.getInstance(getApplicationContext()).findUser(login, password)) {
                Global.isAdmin = true;
                AlertDialog.Builder builder = new AlertDialog.Builder(AuthActivity.this);
                builder.setTitle("Вы успешно вошли");
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(getApplicationContext(), SelectDataTypeActivity.class);
                        startActivity(intent);
                    }

                });
                builder.setNegativeButton(null, null);
                builder.create().show();
            } else {
                AlertDialog.Builder builder = new AlertDialog.Builder(AuthActivity.this);
                builder.setTitle("Ошибка входа");
                builder.setMessage("Попробуйте снова");
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        editTextViewLogin.setText("");
                        editTextViewPassword.setText("");
                    }

                });
                builder.setNegativeButton("Отмена", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }

                });
                builder.create().show();
            }
        }
    };

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        finish();//при нажатии на кнопку назад
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auth);

        //Если мы уже вошли в систему
        if (Global.isAdmin) {
            AlertDialog.Builder builder = new AlertDialog.Builder(AuthActivity.this);
            builder.setTitle("Вход выполнен");
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    //при нажатии сразу выйти к выбору типа данных
                    Intent intent = new Intent(getApplicationContext(), SelectDataTypeActivity.class);
                    startActivity(intent);
                }

            });
            builder.create().show();
        }

        //включить кнопку назад в заголовке
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        editTextViewLogin = findViewById(R.id.editTextViewLogin);
        editTextViewPassword = findViewById(R.id.editTextViewPassword);

        buttonAuth = findViewById(R.id.buttonAuth);
        buttonAuth.setOnClickListener(buttonAuthOnClickListener);
    }
}
