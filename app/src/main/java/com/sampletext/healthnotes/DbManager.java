package com.sampletext.healthnotes;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

public class DbManager extends SQLiteOpenHelper {

    public static final int DATABASE_VERSION = 1;//версия БД

    public static final String DATABASE_NAME = "internalDb";//название БД

    //названия таблиц
    public static final String TABLE_USERS_NAME = "users";
    public static final String TABLE_DISEASES_NAME = "diseases";
    public static final String TABLE_MEDICINES_NAME = "medicines";

    //названия полей таблицы Users
    public static String KEY_USERS_ID = "id";
    public static String KEY_USERS_Login = "login";
    public static String KEY_USERS_Password = "password";

    //названия полей таблицы Diseases
    public static String KEY_DISEASES_ID = "id";
    public static String KEY_DISEASES_NAME = "name";
    public static String KEY_DISEASES_CONTENT = "content";

    //названия полей таблицы Medicines
    public static String KEY_MEDICINES_ID = "id";
    public static String KEY_MEDICINES_NAME = "name";
    public static String KEY_MEDICINES_CONTENT = "content";

    public DbManager(Context context, String name,
                     SQLiteDatabase.CursorFactory factory, int version) {
        super(context, DATABASE_NAME, factory, DATABASE_VERSION);
    }

    //полупаттерн Singleton
    public static DbManager getInstance(Context context) {
        return new DbManager(context, null, null, DATABASE_VERSION);
    }

    //добавление пользователя
    public void insertUser(String login, String password) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_USERS_Login, login);
        values.put(KEY_USERS_Password, password);
        db.insert(TABLE_USERS_NAME, null, values);
        db.close();
    }

    //поиск пользователя
    public boolean findUser(String login, String password) {
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM " + TABLE_USERS_NAME +
                        " WHERE " +
                        KEY_USERS_Login + "=" + "\"" + login + "\"" +
                        " AND " +
                        KEY_USERS_Password + "=" + "\"" + password + "\"",
                null);
        boolean val = cursor.moveToFirst();

        cursor.close();
        db.close();

        return val;
    }

    //получить болезнь по ID
    public DataItem getDiseaseById(int id) {
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.query(TABLE_DISEASES_NAME, null, KEY_DISEASES_ID + "=?", new String[]{Integer.toString(id)}, null, null, null);
        DataItem item = null;
        if (cursor.moveToFirst()) {
            item = new DataItem(cursor.getInt(0), cursor.getString(1), cursor.getString(2));
        }
        cursor.close();
        db.close();
        return item;
    }

    //удалить болезнь
    public void deleteDisease(int id) {
        SQLiteDatabase db = getWritableDatabase();
        db.delete(TABLE_DISEASES_NAME, KEY_DISEASES_ID + "=?", new String[]{Integer.toString(id)});
        db.close();
    }

    //добавить болезнь
    public void insertDisease(String name, String content) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_DISEASES_NAME, name);
        values.put(KEY_DISEASES_CONTENT, content);
        db.insert(TABLE_DISEASES_NAME, null, values);
        db.close();
    }

    //получить все болезни
    public ArrayList<DataItem> diseasesSelectAll() {
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.query(TABLE_DISEASES_NAME, null, null, null, null, null, null);
        ArrayList<DataItem> items = new ArrayList<>();
        while (cursor.moveToNext()) {
            items.add(new DataItem(cursor.getInt(0), cursor.getString(1), cursor.getString(2)));
        }
        cursor.close();
        db.close();
        return items;
    }

    //получить болезни по имени
    public ArrayList<DataItem> diseasesSelectWhereName(String name) {
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.query(TABLE_DISEASES_NAME, null, KEY_DISEASES_NAME + " LIKE ?", new String[]{name}, null, null, null);
        ArrayList<DataItem> items = new ArrayList<>();
        while (cursor.moveToNext()) {
            items.add(new DataItem(cursor.getInt(0), cursor.getString(1), cursor.getString(2)));
        }
        cursor.close();
        db.close();
        return items;
    }

    //обновить болезнь
    public void updateDisease(DataItem item) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_DISEASES_NAME, item.getName());
        values.put(KEY_DISEASES_CONTENT, item.getContent());
        db.update(TABLE_DISEASES_NAME, values, KEY_DISEASES_ID + " = ?", new String[]{Integer.toString(item.getId())});
        db.close();
    }

    //получить лекарство по ID
    public DataItem getMedicineById(int id) {
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.query(TABLE_MEDICINES_NAME, null, KEY_MEDICINES_ID + "=?", new String[]{Integer.toString(id)}, null, null, null);
        DataItem item = null;
        if (cursor.moveToFirst()) {
            item = new DataItem(cursor.getInt(0), cursor.getString(1), cursor.getString(2));
        }
        cursor.close();
        db.close();
        return item;
    }

    //удалить лекарство
    public void deleteMedicine(int id) {
        SQLiteDatabase db = getWritableDatabase();
        db.delete(TABLE_MEDICINES_NAME, KEY_MEDICINES_ID + "=?", new String[]{Integer.toString(id)});
        db.close();
    }

    //добавить лекарство
    public void insertMedicine(String name, String content) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_MEDICINES_NAME, name);
        values.put(KEY_MEDICINES_CONTENT, content);
        db.insert(TABLE_MEDICINES_NAME, null, values);
        db.close();
    }

    //получить все лекарства
    public ArrayList<DataItem> medicinesSelectAll() {
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.query(TABLE_MEDICINES_NAME, null, null, null, null, null, null);
        ArrayList<DataItem> items = new ArrayList<>();
        while (cursor.moveToNext()) {
            items.add(new DataItem(cursor.getInt(0), cursor.getString(1), cursor.getString(2)));
        }
        cursor.close();
        db.close();
        return items;
    }

    //получить лекарства по имени
    public ArrayList<DataItem> medicinesSelectWhereName(String name) {
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.query(TABLE_MEDICINES_NAME, null, KEY_MEDICINES_NAME + " LIKE ?", new String[]{name}, null, null, null);
        ArrayList<DataItem> items = new ArrayList<>();
        while (cursor.moveToNext()) {
            items.add(new DataItem(cursor.getInt(0), cursor.getString(1), cursor.getString(2)));
        }
        cursor.close();
        db.close();
        return items;
    }

    //обновить лекарство
    public void updateMedicine(DataItem item) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_MEDICINES_NAME, item.getName());
        values.put(KEY_MEDICINES_CONTENT, item.getContent());
        db.update(TABLE_MEDICINES_NAME, values, KEY_MEDICINES_ID + " = ?", new String[]{Integer.toString(item.getId())});
        db.close();
    }

    //обработчик создания
    @Override
    public void onCreate(SQLiteDatabase db) {
        //Создаём все 3 таблицы
        db.execSQL(
                "CREATE TABLE " + TABLE_USERS_NAME +
                        " (" + KEY_USERS_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                        + KEY_USERS_Login + " TEXT, "
                        + KEY_USERS_Password + " TEXT)"
        );
        db.execSQL(
                "CREATE TABLE " + TABLE_DISEASES_NAME +
                        " (" + KEY_DISEASES_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                        + KEY_DISEASES_NAME + " TEXT, "
                        + KEY_DISEASES_CONTENT + " TEXT)"
        );
        db.execSQL(
                "CREATE TABLE " + TABLE_MEDICINES_NAME +
                        " (" + KEY_MEDICINES_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                        + KEY_MEDICINES_NAME + " TEXT, "
                        + KEY_MEDICINES_CONTENT + " TEXT)"
        );
    }

    //при обновление базы
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //удаляем все таблицы
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_USERS_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_DISEASES_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_MEDICINES_NAME);

        onCreate(db);//создаём таблицы заново
    }

    //обработчик отката базы
    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        super.onDowngrade(db, oldVersion, newVersion);
        onUpgrade(db, oldVersion, newVersion);//делаем то же что и при обновлении
    }
}
